# OOP analiza_LV6

Analiza laboratorijske vjezbe 6 iz OOP-a

Koristeno nekoliko gotovih linija koda sa MSDN:

https://docs.microsoft.com/en-us/dotnet/api/system.io.file.readalllines?redirectedfrom=MSDN&view=netframework-4.8#System_IO_File_ReadAllLines_System_String_
--> string[] readText = File.ReadAllLines(path);
--> foreach (string s in readText){...}

https://docs.microsoft.com/en-us/dotnet/api/system.random?view=netframework-4.8
https://docs.microsoft.com/en-us/dotnet/api/system.random.next?view=netframework-4.8
--> int randIdx = (new Random()).Next(words.Length);

U igri vjesala dolazi do problema ucitavanja slova umjesto crtica, nisam nikako mogao naci gresku u kodu.


